/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supinfo.SupSalesAnalytics.Utilities;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.supinfo.SupSalesAnalytics.WSDL.Sale;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 *
 * @author Lamine
 */
public class JsonDataManager {

    public JsonDataManager() {
    }
   
    private String LoadData(String urlString) throws Exception {
    BufferedReader reader = null;
    try {
        URL url = new URL(urlString);
        reader = new BufferedReader(new InputStreamReader(url.openStream()));
        StringBuilder buffer = new StringBuilder();
        int read;
        char[] chars = new char[1024];
        while ((read = reader.read(chars)) != -1) {
            buffer.append(chars, 0, read);
        } 

        return buffer.toString();
    } finally {
        if (reader != null) {
            reader.close();
        }
      }  
    }
    
    public List<Sale> ParseJsonData(String urlString) throws Exception{
        String JsonString=this.LoadData(urlString);
          List<Sale> saleList=new ArrayList<Sale>();
        JsonParser parser = new JsonParser();
        JsonElement tradeElement = parser.parse(JsonString);
        JsonArray trade = tradeElement.getAsJsonArray();
          int resultCount = trade.size();
            for (int i = 0; i < resultCount; i++)
            {
               Gson gson =  new GsonBuilder().registerTypeAdapter(XMLGregorianCalendar.class,
                                              new XGCalConverter.Serializer()).registerTypeAdapter(XMLGregorianCalendar.class,
                                                                                                   new XGCalConverter.Deserializer()).create();
               Sale s=new Sale();
               if(gson.fromJson(trade.get(i), Sale.class)!=null) {
                   if(gson.fromJson(trade.get(i), Sale.class).getProduct()!=null) {
                     s.setProduct(gson.fromJson(trade.get(i), Sale.class).getProduct());
                   }
                   if(gson.fromJson(trade.get(i), Sale.class).getCustomer()!=null) {
                        s.setCustomer(gson.fromJson(trade.get(i), Sale.class).getCustomer());
                      }                    
               if(gson.fromJson(trade.get(i), Sale.class).getAmountSold()!=null) {
                        s.setAmountSold(gson.fromJson(trade.get(i), Sale.class).getAmountSold());
                      } 
               if(gson.fromJson(trade.get(i), Sale.class).getChannel()!=null) {
                        s.setChannel(gson.fromJson(trade.get(i), Sale.class).getChannel());
                      }                    
                if(gson.fromJson(trade.get(i), Sale.class).getPromotion()!=null) {
                        s.setPromotion(gson.fromJson(trade.get(i), Sale.class).getPromotion());
                      } 
                if(gson.fromJson(trade.get(i), Sale.class).getQuantitySold()!=null) {
                        s.setQuantitySold(gson.fromJson(trade.get(i), Sale.class).getQuantitySold());
                      } 
                if(gson.fromJson(trade.get(i), Sale.class).getSaleId()!=null) {
                        s.setSaleId(gson.fromJson(trade.get(i), Sale.class).getSaleId());
                      }
                if(gson.fromJson(trade.get(i), Sale.class).getTimeId()!=null) {
                        s.setTimeId(gson.fromJson(trade.get(i), Sale.class).getTimeId());
                      }
                   }            
               saleList.add(s);
            }
                                           
        return saleList;        
    }
}



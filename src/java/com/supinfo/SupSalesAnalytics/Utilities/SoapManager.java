/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supinfo.SupSalesAnalytics.Utilities;

import com.supinfo.SupSalesAnalytics.WSDL.Sale;
import com.supinfo.SupSalesAnalytics.WSDL.SalesExport;
import com.supinfo.SupSalesAnalytics.WSDL.SalesExportService;
import java.util.List;

/**
 *
 * @author Lamine
 */

//Classe qui va gerer les update 
public class SoapManager {
        
     public SoapManager(){                              
     }
 
     public List<Sale> getSales(){
           SalesExportService service=new SalesExportService() ;       
           SalesExport port = service.getSalesExportPort(); 
         return port.getFranceSales(); 
     }
 
}

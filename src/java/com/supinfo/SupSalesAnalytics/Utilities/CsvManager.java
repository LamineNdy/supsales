/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supinfo.SupSalesAnalytics.Utilities;

import com.supinfo.SupSalesAnalytics.WSDL.Sale;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.ParseBigDecimal;
import org.supercsv.cellprocessor.ParseDate;
import org.supercsv.cellprocessor.ParseInt;
import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvBeanReader;
import org.supercsv.io.ICsvBeanReader;
import org.supercsv.prefs.CsvPreference;

/**
 *
 * @author Lamine
 */
public class  CsvManager {
    
    
    private String LoadData(String urlString) throws Exception {
    BufferedReader reader = null;
    try {
        URL url = new URL(urlString);
        reader = new BufferedReader(new InputStreamReader(url.openStream()));
        StringBuilder buffer = new StringBuilder();
        int read;
        char[] chars = new char[1024];
        while ((read = reader.read(chars)) != -1) {
            buffer.append(chars, 0, read);
        } 

        return buffer.toString();
    } finally {
        if (reader != null) {
            reader.close();
        }
      }  
    }
    
    private CellProcessor[] getProcessors() {
                     
        final CellProcessor[] processors = new CellProcessor[] { 
           null,
           new Optional(new ParseInt()),
            new Optional(new ParseBigDecimal()),
            new Optional(new ParseInt()),
            new Optional(new ParseInt()),
            null, 
            null, 
            null, 
           new Optional(new ParseInt()),
          null, 
           null, 
           new Optional(new ParseInt()),
          null, 
          new Optional(new ParseInt()),
          null,
           null,
           null,
          new Optional(new ParseInt()),
          null,                     
          new Optional(new ParseBigDecimal()),
          new Optional(new ParseBigDecimal()),
          null,
          new Optional(new ParseInt()),
          new Optional(new ParseInt()),
         null,  
        null,
          null,
          new Optional(new ParseInt()),
         null, 
         null, 
         null, 
        new Optional(new ParseInt()),
       null, 
       null, 
       new Optional(new ParseInt()),
        null,
        new Optional(new ParseInt()),
        null,
       new Optional(new ParseInt()),
        null, 
       null, 
        new Optional(new ParseInt()),
        null, 
       null, 
        new Optional(new ParseInt()),      
        null, 
        null, 
                null,
         new Optional(new ParseInt()),  
          new Optional(new ParseInt()),  
          null,  
          null, 
       new Optional(new ParseInt()),  
          null, 
         new Optional(new ParseInt()), 
           new Optional(new ParseInt()), 
        null,
        null,
         null,   
          new Optional(new ParseInt()),  
          new Optional(new ParseInt()),  
          null,  
          null, 
           new Optional(new ParseInt()),   
              null, 
             new Optional(new ParseInt()),  
          new Optional(new ParseInt())
        };        
        return processors;
    }
    
    public List<Sale> readWithCsvBeanReader() throws Exception {
        List<Sale> sl=new ArrayList<Sale>();
        ICsvBeanReader beanReader = null;
        try {
            //telecharger le fichier
                      
               InputStream input = new URL("http://supsellertokyo.supinfo.cloudbees.net/sales/export.csv").openStream();                                                  
                beanReader = new CsvBeanReader(new InputStreamReader(input, "UTF-8"), CsvPreference.EXCEL_PREFERENCE);
                
                // the header elements are used to map the values to the bean (names must match)
                final String[] header = beanReader.getHeader(true);
                final CellProcessor[] processors = getProcessors();
                
                Sale sale;
            sale = new Sale();
                while( (sale = beanReader.read(Sale.class, header,processors)) != null ) {
                       sl.add(sale);                  
                }
                
        }
        finally {
                if( beanReader != null ) {
                        beanReader.close();
                }
        }
        return sl;
    }
}

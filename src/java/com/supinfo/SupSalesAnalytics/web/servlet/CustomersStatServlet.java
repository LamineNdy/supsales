/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supinfo.SupSalesAnalytics.web.servlet;

import com.supinfo.SupSalesAnalytics.dao.service.AgencyService;
import com.supinfo.SupSalesAnalytics.dao.service.CustomerService;
import com.supinfo.SupSalesAnalytics.dao.service.SaleService;
import com.supinfo.SupSalesAnalytics.entity.Agency;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 *
 * @author Lamine
 */
@WebServlet(name = "CustomersStatServlet", urlPatterns = {"/Auth/CustomersStat"})
public class CustomersStatServlet extends HttpServlet {

     @EJB
     SaleService saleService;
     
    @EJB
  private AgencyService agencyService;
     
   @EJB
  private CustomerService customerService;
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        //gestion du multi fitler
         request.setAttribute("incomeLevels", this.customerService.getIncomeLevel());
         Map<String,String> params= new HashMap<String, String>();
               if(request.getParameter("gender")!=null)
               {
                   request.getSession().setAttribute("gender", request.getParameter("gender"));
               }
                  
               if( request.getSession().getAttribute("gender")!=null){
                   params.put("custGender",(String) request.getSession().getAttribute("gender"));
               }
               
                   
             if(request.getParameter("marital")!=null)
               {
                   request.getSession().setAttribute("marital", request.getParameter("marital"));
               }
                  
               if( request.getSession().getAttribute("marital")!=null){
                   params.put("custMaritalStatus",(String) request.getSession().getAttribute("marital"));
               }
               
                 List<Agency> agencies=agencyService.findAgency(null);
               request.setAttribute("agencies",agencies);
           
                if(request.getParameter("agency")!=null)
               {
                   request.getSession().setAttribute("agency", request.getParameter("agency"));
               }
                  
               if( request.getSession().getAttribute("agency")!=null){
                   params.put("name",(String) request.getSession().getAttribute("agency"));
               }
                if(request.getParameter("income")!=null)
               {
                   request.getSession().setAttribute("income", request.getParameter("income"));
               }
                  
               if( request.getSession().getAttribute("income")!=null){
                   params.put("custIncomeLevel",(String) request.getSession().getAttribute("income"));
               }
             
               //Envoi des donnees ous format Json
         JSONArray breakdowns = new JSONArray();
         Map<String,Integer> prodToSend=saleService.getBreakDownByCustomerGender(params);         
         for(Map.Entry<String,Integer> entry : prodToSend.entrySet()) {        
              JSONObject sales=new JSONObject(); 
              String updateString = entry.getKey().replace("\"","");
              sales.put(updateString,entry.getValue());              
              breakdowns.add(sales);     
        }       
        request.setAttribute("GenderBreakdowns",breakdowns);
        breakdowns = new JSONArray();
        prodToSend=saleService.getBreakDownByCustomerMarital(params);         
         for(Map.Entry<String,Integer> entry : prodToSend.entrySet()) {        
              JSONObject sales=new JSONObject(); 
              String updateString = entry.getKey().replace("\"","");
              sales.put(updateString,entry.getValue());              
              breakdowns.add(sales);     
        }       
        request.setAttribute("MaritalBreakdowns",breakdowns);
          breakdowns = new JSONArray();
        prodToSend=saleService.getBreakDownByCustomerIncome(params);         
         for(Map.Entry<String,Integer> entry : prodToSend.entrySet()) {        
              JSONObject sales=new JSONObject(); 
              String updateString = entry.getKey().replace("\"","");
              sales.put(updateString,entry.getValue());              
              breakdowns.add(sales);     
        }       
        request.setAttribute("IncomeBreakdowns",breakdowns);
         breakdowns = new JSONArray();
        prodToSend=saleService.getCustomerLoyalty(params);         
         for(Map.Entry<String,Integer> entry : prodToSend.entrySet()) {        
              JSONObject sales=new JSONObject(); 
              String updateString = entry.getKey().replace("\"","");
              sales.put(updateString,entry.getValue());              
              breakdowns.add(sales);     
        }       
        request.setAttribute("customerLoyalty",breakdowns);
         request.getRequestDispatcher("/JSP/customerStat.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
      
    }

   
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supinfo.SupSalesAnalytics.web.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Lamine
 */
@WebServlet(name = "ClearFilterServlet", urlPatterns = {"/Auth/Clear"})
public class ClearFilterServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
 
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        //detruit les variable de sessions utiliser pour le filtre
     if( request.getSession().getAttribute("gender") !=null)
        {
          request.getSession().removeAttribute("gender");        
        }
     if( request.getSession().getAttribute("marital") !=null)
        {
          request.getSession().removeAttribute("marital");        
        }
      if( request.getSession().getAttribute("agency") !=null)
        {
          request.getSession().removeAttribute("agency");        
        }
       if( request.getSession().getAttribute("income") !=null)
        {
          request.getSession().removeAttribute("income");        
        }
       response.sendRedirect(getServletContext().getContextPath()+"/Auth/"+request.getParameter("page"));
    }


  
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}

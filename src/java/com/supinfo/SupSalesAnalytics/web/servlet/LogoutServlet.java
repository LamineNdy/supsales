/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supinfo.SupSalesAnalytics.web.servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Lamine
 */
@WebServlet(name = "LogoutServlet", urlPatterns = {"/Auth/Logout"})
public class LogoutServlet extends HttpServlet {


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        //detruit toute les variable de session pour deconnecter l'user
       request.getSession().removeAttribute("user");   
       
      if( request.getSession().getAttribute("gender") !=null)
        {
          request.getSession().removeAttribute("gender");        
        }
     if( request.getSession().getAttribute("marital") !=null)
        {
          request.getSession().removeAttribute("marital");        
        }
      if( request.getSession().getAttribute("agency") !=null)
        {
          request.getSession().removeAttribute("agency");        
        }
       if( request.getSession().getAttribute("income") !=null)
        {
          request.getSession().removeAttribute("income");        
        }
     response.sendRedirect(getServletContext().getContextPath()+"/Login");
    }

  
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
      
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supinfo.SupSalesAnalytics.web.servlet;

import com.supinfo.SupSalesAnalytics.Utilities.CsvManager;
import com.supinfo.SupSalesAnalytics.Utilities.JsonDataManager;
import com.supinfo.SupSalesAnalytics.Utilities.SoapManager;
import com.supinfo.SupSalesAnalytics.WSDL.Sale;
import com.supinfo.SupSalesAnalytics.dao.service.AgencyService;
import com.supinfo.SupSalesAnalytics.dao.service.CustomerService;
import com.supinfo.SupSalesAnalytics.dao.service.ProductService;
import com.supinfo.SupSalesAnalytics.dao.service.SaleService;
import com.supinfo.SupSalesAnalytics.entity.Agency;
import com.supinfo.SupSalesAnalytics.entity.CustomerEntity;
import com.supinfo.SupSalesAnalytics.entity.ProductEntity;
import com.supinfo.SupSalesAnalytics.entity.SaleEntity;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Lamine
 */
@WebServlet(name = "DateFetcherServlet", urlPatterns = {"/Auth/Fetch"})
public class DateFetcherServlet extends HttpServlet {

      @EJB
   private CustomerService cusTomerService;
  
  @EJB
  private SaleService saleService;

  @EJB
  private ProductService productService;
  
  @EJB
  private AgencyService agencyService;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        /* Premier lancement de l"application*/
         //creation des agence si elle n'existe pas
        this.InitAgency(); 
                         
       //recuperation des mise a jours des donnees
     //   this.getUpdate();
       
     response.sendRedirect(getServletContext().getContextPath()+"/Auth/Dashboard");
    }

 
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       
    }

   
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
    
      //Import des donnees SOAP au demarrage de l'application
     public void importProducts(List<Sale> sales){
        if(sales!=null) {
             for (Sale saleEnt : sales)
               {                  
                if (saleEnt!=null)
                {
                    ProductEntity pe=new ProductEntity();
                    //verification de l'existence du produit en base
                   if(this.productService.findProduct(saleEnt.getProduct().getProdId(),null).isEmpty()){                    
                    pe.setProdId(saleEnt.getProduct().getProdId());  
                    pe.setProdListPrice(saleEnt.getProduct().getProdListPrice());
                    pe.setProdMinPrice(saleEnt.getProduct().getProdMinPrice());
                    pe.setProdName(saleEnt.getProduct().getProdName());        
                    productService.addProduct(pe);   
                    }
                }                         
              }
         } 
    }    
    public void importCustomers(List<Sale> sales){
     for (Sale saleEnt : sales)
         {                  
             if (saleEnt!=null)
             {                 
                 CustomerEntity ce=new CustomerEntity();
                 //verification de l'existence du customer en base
                if(this.cusTomerService.findCustomer(saleEnt.getCustomer().getCustId(),null).isEmpty()){                 
                 ce.setCustFirstName(saleEnt.getCustomer().getCustFirstName());
                 ce.setCustGender(saleEnt.getCustomer().getCustGender());
                 ce.setCustId(saleEnt.getCustomer().getCustId());
                 ce.setCustIncomeLevel(saleEnt.getCustomer().getCustIncomeLevel());
                 ce.setCustLastName(saleEnt.getCustomer().getCustLastName());
                 ce.setCustMaritalStatus(saleEnt.getCustomer().getCustMaritalStatus());               
                 cusTomerService.addCustomer(ce);
                }
             }    
         }
    }
    
    public void importSales(List<Sale> sales,String agency){
        for (Sale saleEnt : sales)
         {                  
             if (saleEnt!=null)
             {
                    SaleEntity se=new SaleEntity();
                    //verification de l'existence de la vente en base
                if(this.saleService.findSales(saleEnt.getSaleId(),null).isEmpty()){                    
                    se.setAmountSold(saleEnt.getAmountSold());
                    se.setChannel(saleEnt.getChannel().getChannelDesc());
                    se.setQuantitySold(saleEnt.getAmountSold());
                    se.setSaleId(saleEnt.getSaleId());
                    se.setAgency(agencyService.findAgency(agency).get(0));
                   List<CustomerEntity> customers=this.cusTomerService.findCustomer(saleEnt.getCustomer().getCustId(),null);
                   if(!customers.isEmpty()) {
                       for(CustomerEntity ce : customers){
                           if(saleEnt.getCustomer().getCustId().intValue()==ce.getCustId().intValue()){
                         se.setCustomer(ce);
                         break;
                     }
                       }
                     
                 }
                   List<ProductEntity> products= this.productService.findProduct(saleEnt.getProduct().getProdId(),null);
                   if(!products.isEmpty()){
                       for(ProductEntity pe: products) {
                           if(saleEnt.getProduct().getProdId().intValue()==pe.getProdId().intValue()){
                               se.setProduct(pe);
                               break;
                           }
                       }
                   }
                   
                 saleService.addSale(se);
             }    
           }
         }
    }
        
   
   private void InitAgency(){
       if(agencyService.findAgency("Paris").isEmpty()){
           agencyService.addGency(new Agency("Paris"));
       }
        if(agencyService.findAgency("Montreal").isEmpty()){
           agencyService.addGency(new Agency("Montreal"));
       }
         if(agencyService.findAgency("Tokyo").isEmpty()){
           agencyService.addGency(new Agency("Tokyo"));
       }
   }
   
   private void getUpdate(){
       SoapManager sp=new SoapManager();    
       
       //Paris
       this.importProducts(sp.getSales());
       this.importCustomers(sp.getSales());
       this.importSales(sp.getSales(),"Paris");      
       
       // Montreal
        
        JsonDataManager jm=new JsonDataManager();
        List<Sale> saleMtl=new ArrayList<Sale>();
        try {        
            saleMtl=jm.ParseJsonData("http://supsellermontreal.supinfo.cloudbees.net/sales/export.json");
             this.importProducts(saleMtl);
             this.importCustomers(saleMtl);
             this.importSales(saleMtl,"Montreal");
        } catch (Exception ex) {
            Logger.getLogger(LoginServlet.class.getName()).log(Level.SEVERE, null, ex);
       }
        
        
       //Tokyo(Echec)
              
          CsvManager csvManager=new CsvManager();
          List<Sale> saleTky=new ArrayList<Sale>();
        try {
            saleTky=csvManager.readWithCsvBeanReader();        
             this.importProducts(saleTky);
             this.importCustomers(saleTky);
             this.importSales(saleTky,"Tokyo");
        } catch (Exception ex) {
            Logger.getLogger(LoginServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
       
      
      
  } 
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supinfo.SupSalesAnalytics.web.servlet;

import com.supinfo.SupSalesAnalytics.dao.service.AgencyService;
import com.supinfo.SupSalesAnalytics.dao.service.CustomerService;
import com.supinfo.SupSalesAnalytics.dao.service.SaleService;
import com.supinfo.SupSalesAnalytics.entity.Agency;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Lamine
 */
@WebServlet(name = "DashboardServlet", urlPatterns = {"/Auth/Dashboard"})
public class DashboardServlet extends HttpServlet {
  
  
  @EJB
  private SaleService saleService;

  
 @EJB
  private AgencyService agencyService;
  
  @EJB
  private CustomerService customerService;
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         //filters elements 
        
         request.setAttribute("incomeLevels", this.customerService.getIncomeLevel());
         
           Map<String,String> params= new HashMap<String, String>();
               if(request.getParameter("gender")!=null)
               {
                   request.getSession().setAttribute("gender", request.getParameter("gender"));
               }
                  
               if( request.getSession().getAttribute("gender")!=null){
                   params.put("custGender",(String) request.getSession().getAttribute("gender"));
               }
               
             if(request.getParameter("marital")!=null)
               {
                   request.getSession().setAttribute("marital", request.getParameter("marital"));
               }
                  
               if( request.getSession().getAttribute("marital")!=null){
                   params.put("custMaritalStatus",(String) request.getSession().getAttribute("marital"));
               }
               
                 List<Agency> agencies=agencyService.findAgency(null);
               request.setAttribute("agencies",agencies);
            
                if(request.getParameter("agency")!=null)
               {
                   request.getSession().setAttribute("agency", request.getParameter("agency"));
               }
                  
               if( request.getSession().getAttribute("agency")!=null){
                   params.put("name",(String) request.getSession().getAttribute("agency"));
               }
             
                 if(request.getParameter("income")!=null)
               {
                   request.getSession().setAttribute("income", request.getParameter("income"));
               }
                  
               if( request.getSession().getAttribute("income")!=null){
                   params.put("custIncomeLevel",(String) request.getSession().getAttribute("income"));
               }
               
            //envoi des donnees a la servlet
      request.setAttribute("AmountSold",saleService.getSoldAmount(params));
      request.setAttribute("SalesMade",saleService.getSoldQuantity(params));
      request.getRequestDispatcher("/JSP/Dashboard.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       // processRequest(request, response);
    }


    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supinfo.SupSalesAnalytics.web.servlet;

import com.supinfo.SupSalesAnalytics.dao.service.LoginService;
import com.supinfo.SupSalesAnalytics.entity.BankAdvisor;
import java.io.IOException;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Lamine
 */
@WebServlet(name = "LoginServlet", urlPatterns = {"/Login"})
public  class LoginServlet extends HttpServlet {

  @EJB
  private LoginService loginService;
  

   @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException { 
       //creation d'un bank advisor email=test@supinfo.com et mot de passe=passer
                this.InitBankAdvisor();           
       req.getRequestDispatcher("/JSP/login.jsp").forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
     
        String email = req.getParameter("email");
        String password = req.getParameter("password");
                      
        List<BankAdvisor> bankAdvisors=loginService.getAllBankAdvisor();         
        for (BankAdvisor bankAdvisor : bankAdvisors) {
            if (bankAdvisor.getEmail().equals(email) && (bankAdvisor.getPassword()==password.hashCode())){
                req.getSession().setAttribute("user", email);
                resp.sendRedirect(getServletContext().getContextPath()+"/Auth/Fetch");
                break;
                }             
            else {
                doGet(req, resp);
            }                                                                  
        }
     }
    
    private void InitBankAdvisor(){
        boolean found=false; 
      for(BankAdvisor b :loginService.getAllBankAdvisor()){
          if(b.getEmail().equals("test@supinfo.com")){
              found=true;
          }
      }
      if(!found) {
           loginService.addBankAdvisor(new BankAdvisor("test@supinfo.com","passer"));
       }
   }
}

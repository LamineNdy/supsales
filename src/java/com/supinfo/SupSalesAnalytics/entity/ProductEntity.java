/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supinfo.SupSalesAnalytics.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author Lamine
 */
@Entity
public class ProductEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;   
    private Integer prodId;
    private BigDecimal prodListPrice;
    private BigDecimal prodMinPrice;
    private String prodName;  

    public ProductEntity(Integer prodId, BigDecimal prodListPrice, BigDecimal prodMinPrice, String prodName) {
        this.prodId = prodId;
        this.prodListPrice = prodListPrice;
        this.prodMinPrice = prodMinPrice;
        this.prodName = prodName;
    }

    
    public ProductEntity() {
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ProductEntity)) {
            return false;
        }
        ProductEntity other = (ProductEntity) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.supinfo.SupSalesAnalytics.entity.ProductEntity[ id=" + id + " ]";
    }

   
    /**
     * @return the prodId
     */
    public Integer getProdId() {
        return prodId;
    }

    /**
     * @param prodId the prodId to set
     */
    public void setProdId(Integer prodId) {
        this.prodId = prodId;
    }

    /**
     * @return the prodListPrice
     */
    public BigDecimal getProdListPrice() {
        return prodListPrice;
    }

    /**
     * @param prodListPrice the prodListPrice to set
     */
    public void setProdListPrice(BigDecimal prodListPrice) {
        this.prodListPrice = prodListPrice;
    }

    /**
     * @return the prodMinPrice
     */
    public BigDecimal getProdMinPrice() {
        return prodMinPrice;
    }

    /**
     * @param prodMinPrice the prodMinPrice to set
     */
    public void setProdMinPrice(BigDecimal prodMinPrice) {
        this.prodMinPrice = prodMinPrice;
    }

    /**
     * @return the prodName
     */
    public String getProdName() {
        return prodName;
    }

    /**
     * @param prodName the prodName to set
     */
    public void setProdName(String prodName) {
        this.prodName = prodName;
    }
}

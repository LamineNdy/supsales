/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supinfo.SupSalesAnalytics.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

/**
 *
 * @author Lamine
 */
@Entity
public class SaleEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private BigDecimal amountSold;   
    private String channel;
    @OneToOne
    private CustomerEntity customer;
    @OneToOne
    private ProductEntity product;    
    private BigDecimal quantitySold;       
    private Integer saleId;  
    @OneToOne
    private Agency agency;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public SaleEntity(BigDecimal amountSold, String channel, CustomerEntity customer, ProductEntity product, BigDecimal quantitySold, Integer saleId,Agency agency) {
        this.amountSold = amountSold;
        this.channel = channel;
        this.customer = customer;
        this.product = product;
        this.quantitySold = quantitySold;
        this.saleId = saleId;
        this.agency=agency;
    }

    public SaleEntity() {
    }

    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SaleEntity)) {
            return false;
        }
        SaleEntity other = (SaleEntity) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.supinfo.SupSalesAnalytics.entity.SaleEntity[ id=" + id + " ]";
    }

    /**
     * @return the amountSold
     */
    public BigDecimal getAmountSold() {
        return amountSold;
    }

    /**
     * @param amountSold the amountSold to set
     */
    public void setAmountSold(BigDecimal amountSold) {
        this.amountSold = amountSold;
    }

    /**
     * @return the channelId
     */
    public String getChannel() {
        return channel;
    }

    /**
     * @param channelId the channelId to set
     */
    public void setChannel(String channel) {
        this.channel = channel;
    }

    /**
     * @return the customer
     */
    public CustomerEntity getCustomer() {
        return customer;
    }

    /**
     * @param customer the customer to set
     */
    public void setCustomer(CustomerEntity customer) {
        this.customer = customer;
    }

    /**
     * @return the product
     */
    public ProductEntity getProduct() {
        return product;
    }

    /**
     * @param product the product to set
     */
    public void setProduct(ProductEntity product) {
        this.product = product;
    }

    /**
     * @return the quantitySold
     */
    public BigDecimal getQuantitySold() {
        return quantitySold;
    }

    /**
     * @param quantitySold the quantitySold to set
     */
    public void setQuantitySold(BigDecimal quantitySold) {
        this.quantitySold = quantitySold;
    }

    /**
     * @return the saleId
     */
    public Integer getSaleId() {
        return saleId;
    }

    /**
     * @param saleId the saleId to set
     */
    public void setSaleId(Integer saleId) {
        this.saleId = saleId;
    }

    /**
     * @return the agency
     */
    public Agency getAgency() {
        return agency;
    }

    /**
     * @param agency the agency to set
     */
    public void setAgency(Agency agency) {
        this.agency = agency;
    }
    
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supinfo.SupSalesAnalytics.entity;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 *
 * @author Lamine
 */
@Entity
public class CustomerEntity implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;           
    private String custFirstName;
    private String custLastName;  
    private String custGender; 
    private String custMaritalStatus;    
    private Integer custId;
    private String custIncomeLevel;    
    

    public CustomerEntity(String custFirstName, String custGender, Integer custId, String custIncomeLevel, String custLastName, String custMaritalStatus) {
        this.custFirstName = custFirstName;
        this.custGender = custGender;
        this.custId = custId;
        this.custIncomeLevel = custIncomeLevel;
        this.custLastName = custLastName;
        this.custMaritalStatus = custMaritalStatus;   
    }

    
    public CustomerEntity() {
    }
    

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (getId() != null ? getId().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CustomerEntity)) {
            return false;
        }
        CustomerEntity other = (CustomerEntity) object;
        if ((this.getId() == null && other.getId() != null) || (this.getId() != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.supinfo.SupSalesAnalytics.entity.CustomerEntity[ id=" + getId() + " ]";
    }

    /**
     * @return the custFirstName
     */
    public String getCustFirstName() {
        return custFirstName;
    }

    /**
     * @param custFirstName the custFirstName to set
     */
    public void setCustFirstName(String custFirstName) {
        this.custFirstName = custFirstName;
    }

    /**
     * @return the custGender
     */
    public String getCustGender() {
        return custGender;
    }

    /**
     * @param custGender the custGender to set
     */
    public void setCustGender(String custGender) {
        this.custGender = custGender;
    }

    /**
     * @return the custId
     */
    public Integer getCustId() {
        return custId;
    }

    /**
     * @param custId the custId to set
     */
    public void setCustId(Integer custId) {
        this.custId = custId;
    }

    /**
     * @return the custIncomeLevel
     */
    public String getCustIncomeLevel() {
        return custIncomeLevel;
    }

    /**
     * @param custIncomeLevel the custIncomeLevel to set
     */
    public void setCustIncomeLevel(String custIncomeLevel) {
        this.custIncomeLevel = custIncomeLevel;
    }

    /**
     * @return the custLastName
     */
    public String getCustLastName() {
        return custLastName;
    }

    /**
     * @param custLastName the custLastName to set
     */
    public void setCustLastName(String custLastName) {
        this.custLastName = custLastName;
    }

    /**
     * @return the custMaritalStatus
     */
    public String getCustMaritalStatus() {
        return custMaritalStatus;
    }

    /**
     * @param custMaritalStatus the custMaritalStatus to set
     */
    public void setCustMaritalStatus(String custMaritalStatus) {
        this.custMaritalStatus = custMaritalStatus;
    }
   
  
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supinfo.SupSalesAnalytics.dao;

import com.supinfo.SupSalesAnalytics.entity.CustomerEntity;
import java.util.List;
import java.util.Map;
import javax.ejb.Local;

/**
 *
 * @author Lamine
 */

@Local
public interface CustomerDao {
    
    void addCustomer(CustomerEntity cu);  
    List<CustomerEntity> findCustomer(Integer custId,Map<String,String> parameter);
    List<String> getIncomeLevel();
}

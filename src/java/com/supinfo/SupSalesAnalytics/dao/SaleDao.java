/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supinfo.SupSalesAnalytics.dao;

import com.supinfo.SupSalesAnalytics.entity.SaleEntity;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import javax.ejb.Local;

/**
 *
 * @author Lamine
 */
@Local
public interface SaleDao {
      void addSale(SaleEntity sale);
      List<SaleEntity> findSales(Integer saleId,Map<String,String> parameters);
      BigDecimal getSoldAmount(Map<String,String> parameters);
      int getSoldQuantity(Map<String,String> parameters);  
      Map<String,Integer> getChannelBreakDown(Map<String,String> parameters);
      Map<String,Integer> getAgencyBreakDown(Map<String,String> parameters);
      Map<String,Integer> getBreakDownByCustomerGender(Map<String,String> parameters);
      Map<String,Integer> getBreakDownByCustomerMarital(Map<String,String> parameters);
      Map<String,Integer> getBreakDownByCustomerIncome(Map<String,String> parameters);
      Map<String,Integer> getCustomerLoyalty(Map<String,String> parameters);
      Map<String,Integer> getTopProducts(Map<String,String> parameters);

}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supinfo.SupSalesAnalytics.dao;

import com.supinfo.SupSalesAnalytics.entity.ProductEntity;
import java.util.List;
import java.util.Map;
import javax.ejb.Local;

/**
 *
 * @author Lamine
 */

@Local
public interface ProductDao {
    void addProduct(ProductEntity product);  
    List<ProductEntity> findProduct(Integer prodId,Map<String, String> parameter);
    
}

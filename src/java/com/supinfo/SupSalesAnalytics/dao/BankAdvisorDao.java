/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supinfo.SupSalesAnalytics.dao;

import com.supinfo.SupSalesAnalytics.entity.BankAdvisor;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Lamine
 */

@Local
public interface BankAdvisorDao {
   // BankAdvisor findBankAdvisor (String Email);   
   // public BankAdvisor findBankAdvisor(int i);
     void addBankAdvisor(BankAdvisor bankAdvisor);
     List<BankAdvisor> getAllBankAdvisor();
}

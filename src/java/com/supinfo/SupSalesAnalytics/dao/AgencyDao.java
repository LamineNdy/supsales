/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supinfo.SupSalesAnalytics.dao;

import com.supinfo.SupSalesAnalytics.entity.Agency;
import java.util.List;
import javax.ejb.Local;

/**
 *
 * @author Lamine
 */

@Local
public interface AgencyDao {
    List<Agency> findAgency(String name);
    void addGency(Agency a);
}

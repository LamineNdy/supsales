/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supinfo.SupSalesAnalytics.dao.jpa;

import com.supinfo.SupSalesAnalytics.dao.SaleDao;
import com.supinfo.SupSalesAnalytics.dao.service.AgencyService;
import com.supinfo.SupSalesAnalytics.dao.service.CustomerService;
import com.supinfo.SupSalesAnalytics.dao.service.ProductService;
import com.supinfo.SupSalesAnalytics.entity.Agency;
import com.supinfo.SupSalesAnalytics.entity.CustomerEntity;
import com.supinfo.SupSalesAnalytics.entity.ProductEntity;
import com.supinfo.SupSalesAnalytics.entity.SaleEntity;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 *
 * @author Lamine
 */

@Stateless
public class JpaSaleDao implements SaleDao{
    
   @PersistenceContext
    private EntityManager em;
   
   @EJB
   private ProductService productService;
   
   @EJB
   private CustomerService customerService;
   
   @EJB
   private AgencyService agencyService;
   
    @Override
    public void addSale(SaleEntity sale) {
      em.persist(sale);
    }

    @Override
    public List<SaleEntity> findSales(Integer saleId,Map<String,String> parameters) {
          CriteriaBuilder cb = em.getCriteriaBuilder();
        // On instancie une nouvelle query (Renvoyant un/des objets "Trip")
        CriteriaQuery<SaleEntity> query = cb.createQuery(SaleEntity.class);
        // On définie quelle classe (mappée sur une table) sera utilisé pour
        // comparer la valeur d'une colonne avec notre paramètre.
        Root<SaleEntity> SaleTable = query.from(SaleEntity.class);
         List<Predicate> predicates = new ArrayList<Predicate>();
         if(saleId!=null) {
            predicates.add(cb.equal(SaleTable.get("saleId").as(Integer.class), saleId));
        }
          if(parameters!=null){
                 for(Map.Entry<String,String> entry : parameters.entrySet()) {  
                  if(entry.getKey()!=null && entry.getValue()!=null){ 
                      if(!entry.getKey().equals("name")) {
                          predicates.add(cb.equal(SaleTable.get("customer").get(entry.getKey()).as(String.class), entry.getValue()));
                      }
                      else{
                           predicates.add(cb.equal(SaleTable.get("agency").get(entry.getKey()).as(String.class), entry.getValue()));
                      }
                  }                  
                 }
         }
            // On ajoute le tableau de predicat à la clause "where" de la query.
        query.where(predicates.toArray(new Predicate[predicates.size()]));
        
          // On execute la query
        List<SaleEntity> results = em.createQuery(query).getResultList();       
        return results;
    }

    @Override
    public BigDecimal getSoldAmount(Map<String,String> parameters) {        
          BigDecimal amount=new BigDecimal(0);
//          Map<String,String> map1= new HashMap<String, String>();
//          map1.put("custGender", "F");
          List<SaleEntity> ses=this.findSales(null,parameters);
          for(SaleEntity saleEntity : ses ){
              amount=amount.add(saleEntity.getAmountSold());
          }
          return amount;
    }

    @Override
    public int getSoldQuantity(Map<String,String> parameters) {
         int soldQuantity=0;
//          Map<String,String> map1= new HashMap<String, String>();
//           map1.put("channel", "Direct Sales");
          List<SaleEntity> ses=this.findSales(null,parameters);
          for(SaleEntity saleEntity : ses ){
             soldQuantity++;
          }
          return soldQuantity;
    }

    @Override
    public Map<String, Integer> getChannelBreakDown(Map<String,String> parameters) {
        Map<String,Integer> map= new HashMap<String, Integer>();
        List<SaleEntity> ses=this.findSales(null,parameters);
        List<String> channelList=em.createQuery("SELECT s.channel FROM SaleEntity s").getResultList();
       for(String ch : channelList){ 
            int sold=0;
               for(SaleEntity saleEntity : ses){  
                   
                   if(saleEntity.getProduct()!=null) {
                       if(ch.equals(saleEntity.getChannel())){
                           sold++;
                   }
                }                 
               
          }
                 if(sold>=0){
                     if(ch!=null) {
                         map.put(ch, sold);
                     }                   
            }
       }
          
           return map;
    }

    @Override
    public Map<String,Integer> getTopProducts(Map<String,String> parameters) {     
         Map<String,Integer> map= new HashMap<String, Integer>();
       List<ProductEntity> products=productService.findProduct(null, null);      
        List<SaleEntity> ses=this.findSales(null,parameters);
          for(ProductEntity pe : products ){     
              int sold=0;
               for(SaleEntity saleEntity : ses){  
                   if(saleEntity.getProduct()!=null) {
                       if(saleEntity.getProduct().getProdId().intValue()==pe.getProdId().intValue()){
                           sold++;
                   }
                }
          }
               if(sold>=0){
                   if(pe.getProdName()!=null) {
                       map.put(pe.getProdName(), sold);
                   }                   
               }
                   
        }      
       return map;
    }
    
    @Override
    public Map<String, Integer> getAgencyBreakDown(Map<String,String> parameters) {
          Map<String,Integer> map= new HashMap<String, Integer>();
       List<Agency> agencies=agencyService.findAgency(null);      
        List<SaleEntity> ses=this.findSales(null,parameters);
          for(Agency a : agencies ){     
              int sold=0;
               for(SaleEntity saleEntity : ses){  
                   if(saleEntity.getProduct()!=null) {
                       if(saleEntity.getAgency().getId().intValue()==a.getId().intValue()){
                           sold++;
                   }
                }
          }
               if(sold>=0){
                   if(a.getName()!=null) {
                       map.put(a.getName(), sold);
                   }                   
               }
                   
        }      
       return map;
    }

    @Override
    public Map<String, Integer> getBreakDownByCustomerGender(Map<String,String> parameters) {
       Map<String,Integer> map= new HashMap<String, Integer>();
       List<CustomerEntity> customers=customerService.findCustomer(null, null);      
        List<SaleEntity> ses=this.findSales(null,parameters);
          for(CustomerEntity c : customers ){     
              int sold=0;
               for(SaleEntity saleEntity : ses){  
                   if(saleEntity.getCustomer()!=null) {
                       if(saleEntity.getCustomer().getCustGender().equals(c.getCustGender())){
                           sold++;
                   }
                }
          }
               if(sold>=0){
                   if(c.getCustGender()!=null) {
                       map.put(c.getCustGender(), sold);
                   }                   
               }
                   
        }      
       return map;
    }

    @Override
    public Map<String, Integer> getBreakDownByCustomerMarital(Map<String,String> parameters) {
          Map<String,Integer> map= new HashMap<String, Integer>();
       List<CustomerEntity> customers=customerService.findCustomer(null, null);      
        List<SaleEntity> ses=this.findSales(null,parameters);
           for(CustomerEntity c : customers ){     
              int sold=0;
               for(SaleEntity saleEntity : ses){  
                   if(saleEntity.getCustomer()!=null && saleEntity.getCustomer().getCustMaritalStatus()!=null) {
                       if(saleEntity.getCustomer().getCustMaritalStatus().equals(c.getCustMaritalStatus())){
                           sold++;
                   }
                }
          }
               if(sold>=0){
                   if(c.getCustMaritalStatus()!=null) {
                       map.put(c.getCustMaritalStatus(), sold);
                   }                   
               }
                   
        }      
       return map;
    }

    @Override
    public Map<String, Integer> getBreakDownByCustomerIncome(Map<String,String> parameters) {
       Map<String,Integer> map= new HashMap<String, Integer>();
       List<CustomerEntity> customers=customerService.findCustomer(null, null);      
        List<SaleEntity> ses=this.findSales(null,parameters);
          for(CustomerEntity c : customers ){     
              int sold=0;
               for(SaleEntity saleEntity : ses){  
                   if(saleEntity.getCustomer()!=null) {
                        if(saleEntity.getCustomer().getCustIncomeLevel()!=null) {
                           if(saleEntity.getCustomer().getCustIncomeLevel().equals(c.getCustIncomeLevel())){
                               sold++;
                       }
                       }
                }
          }
               if(sold>=0){
                    if(c.getCustIncomeLevel()!=null) {
                       map.put(c.getCustIncomeLevel(), sold);
                   }                   
               }
                   
        }      
       return map;
    }

    @Override
    public Map<String, Integer> getCustomerLoyalty(Map<String,String> parameters) {
      Map<String,Integer> map= new HashMap<String, Integer>();
       List<CustomerEntity> customers=customerService.findCustomer(null, null);      
        List<SaleEntity> ses=this.findSales(null,parameters);
          for(CustomerEntity c : customers ){     
              int sold=0;
               for(SaleEntity saleEntity : ses){  
                   if(saleEntity.getCustomer()!=null) {
                        if(saleEntity.getCustomer().getCustId()!=null) {
                           if(saleEntity.getCustomer().getCustId().intValue()==c.getCustId().intValue()){
                               sold++;
                       }
                       }
                }
          }
               if(sold>=0){
                    if(c.getCustIncomeLevel()!=null) {
                        if(c.getCustFirstName()!=null && c.getCustLastName()!=null) {
                            map.put(c.getCustFirstName() +" "+ c.getCustLastName(), sold);
                        }
                   }                   
               }
                   
        }      
       return map;
    }
    
}

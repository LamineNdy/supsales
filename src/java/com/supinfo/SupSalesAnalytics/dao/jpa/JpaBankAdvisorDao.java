/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supinfo.SupSalesAnalytics.dao.jpa;

import com.supinfo.SupSalesAnalytics.dao.BankAdvisorDao;
import com.supinfo.SupSalesAnalytics.entity.BankAdvisor;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;


/**
 *
 * @author Lamine
 */
@Stateless
public class JpaBankAdvisorDao implements BankAdvisorDao{
    
    @PersistenceContext
    private EntityManager em;

    @Override
    public void addBankAdvisor(BankAdvisor ba) {
       em.persist(ba);
       //return ba;
    }

    @Override
    public List<BankAdvisor> getAllBankAdvisor() {
       return em.createQuery("SELECT b FROM BankAdvisor b").getResultList();
    }
    
}

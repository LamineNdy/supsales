/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supinfo.SupSalesAnalytics.dao.jpa;

import com.supinfo.SupSalesAnalytics.dao.AgencyDao;
import com.supinfo.SupSalesAnalytics.entity.Agency;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 *
 * @author Lamine
 */

@Stateless
public class JpaAgencyDao implements AgencyDao{

    @PersistenceContext
    EntityManager em;
    @Override
    public List<Agency> findAgency(String name) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        // On instancie une nouvelle query (Renvoyant un/des objets "Trip")
        CriteriaQuery<Agency> query = cb.createQuery(Agency.class);
        // On définie quelle classe (mappée sur une table) sera utilisé pour
        // comparer la valeur d'une colonne avec notre paramètre.
        Root<Agency> AgencyTable = query.from(Agency.class);
         List<Predicate> predicates = new ArrayList<Predicate>();
         if(name!=null) {
            predicates.add(cb.equal(AgencyTable.get("name").as(String.class), name));
        }
            // On ajoute le tableau de predicat à la clause "where" de la query.
        query.where(predicates.toArray(new Predicate[predicates.size()]));
        
          // On execute la query
        List<Agency> results = em.createQuery(query).getResultList();       
        return results;
    }

    @Override
    public void addGency(Agency a) {
        em.persist(a);
    }
    
}

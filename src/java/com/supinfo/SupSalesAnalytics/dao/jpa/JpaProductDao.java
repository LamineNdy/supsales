/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supinfo.SupSalesAnalytics.dao.jpa;

import com.supinfo.SupSalesAnalytics.dao.ProductDao;
import com.supinfo.SupSalesAnalytics.entity.ProductEntity;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 *
 * @author Lamine
 */

@Stateless
public class JpaProductDao implements ProductDao{

    @PersistenceContext
    private EntityManager em;
    
    @Override
    public void addProduct(ProductEntity product) {
        em.persist(product);
    }

    @Override
    public List<ProductEntity> findProduct(Integer prodId,Map<String, String> parameters) {
           // Creation du criteriaBuilder (pour construire la query)
        CriteriaBuilder cb = em.getCriteriaBuilder();
        // On instancie une nouvelle query (Renvoyant un/des objets "Trip")
        CriteriaQuery<ProductEntity> query = cb.createQuery(ProductEntity.class);
        // On définie quelle classe (mappée sur une table) sera utilisé pour
        // comparer la valeur d'une colonne avec notre paramètre.
        Root<ProductEntity> productTable = query.from(ProductEntity.class);
         List<Predicate> predicates = new ArrayList<Predicate>();
         if(prodId!=null) {
            predicates.add(cb.equal(productTable.get("prodId").as(Integer.class), prodId));
        }
         if(parameters!=null){
                 for(Map.Entry<String,String> entry : parameters.entrySet()) {  
                  if(entry.getKey()!=null && entry.getValue()!=null){
                       predicates.add(cb.equal(productTable.get(entry.getKey()).as(String.class), entry.getValue()));
                  }   
                 }
         }
            // On ajoute le tableau de predicat à la clause "where" de la query.
        query.where(predicates.toArray(new Predicate[predicates.size()]));
        
          // On execute la query
        List<ProductEntity> results = em.createQuery(query).getResultList();       
        return results;
    }
        
}

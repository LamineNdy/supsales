/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supinfo.SupSalesAnalytics.dao.jpa;

import com.supinfo.SupSalesAnalytics.dao.CustomerDao;
import com.supinfo.SupSalesAnalytics.entity.CustomerEntity;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

/**
 *
 * @author Lamine
 */

@Stateless
public class JpaCustomerDao implements CustomerDao {

    @PersistenceContext
    EntityManager em;
    
    @Override
    public void addCustomer(CustomerEntity cu) {
       em.persist(cu);
    }

    @Override
    public List<CustomerEntity> findCustomer(Integer custId,Map<String,String> parameters) {
            // Creation du criteriaBuilder (pour construire la query)
        CriteriaBuilder cb = em.getCriteriaBuilder();
        // On instancie une nouvelle query (Renvoyant un/des objets "Trip")
        CriteriaQuery<CustomerEntity> query = cb.createQuery(CustomerEntity.class);
        // On définie quelle classe (mappée sur une table) sera utilisé pour
        // comparer la valeur d'une colonne avec notre paramètre.
        Root<CustomerEntity> CustomerTable = query.from(CustomerEntity.class);
         List<Predicate> predicates = new ArrayList<Predicate>();
         if(custId!=null) {
            predicates.add(cb.equal(CustomerTable.get("custId").as(Integer.class), custId));
        }
          if(parameters!=null){
                 for(Map.Entry<String,String> entry : parameters.entrySet()) {  
                  if(entry.getKey()!=null && entry.getValue()!=null){
                       predicates.add(cb.equal(CustomerTable.get(entry.getKey()).as(String.class), entry.getValue()));
                  }   
                 }
         }
            // On ajoute le tableau de predicat à la clause "where" de la query.
        query.where(predicates.toArray(new Predicate[predicates.size()]));
        
          // On execute la query
        List<CustomerEntity> results = em.createQuery(query).getResultList();       
        return results;
    }

    @Override
    public List<String> getIncomeLevel() {
       return em.createQuery("SELECT DISTINCT c.custIncomeLevel FROM CustomerEntity c").getResultList();
    }
    
}

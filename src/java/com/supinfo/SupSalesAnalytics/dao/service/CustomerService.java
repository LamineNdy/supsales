/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supinfo.SupSalesAnalytics.dao.service;

import com.supinfo.SupSalesAnalytics.dao.CustomerDao;
import com.supinfo.SupSalesAnalytics.entity.CustomerEntity;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Lamine
 */

@Stateless
public class CustomerService {
    
    @EJB
    CustomerDao customerDao;
    
    public void addCustomer(CustomerEntity ce){
        customerDao.addCustomer(ce);
    }
    
   public List<CustomerEntity> findCustomer(Integer custId,Map<String,String> parameter){
        return customerDao.findCustomer(custId,parameter);
    }
   public List<String> getIncomeLevel(){
       return customerDao.getIncomeLevel();
   }
}

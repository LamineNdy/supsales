/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supinfo.SupSalesAnalytics.dao.service;

import com.supinfo.SupSalesAnalytics.dao.AgencyDao;
import com.supinfo.SupSalesAnalytics.entity.Agency;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

/**
 *
 * @author Lamine
 */
@Path("/agencies")
@Stateless
public class AgencyService {
    @EJB
    AgencyDao agencyDao;
    
     @GET @Path("/all")  @Produces(MediaType.APPLICATION_XML)  
      public List<Agency> findAgency(){
          return agencyDao.findAgency(null);
      }
     
      public List<Agency> findAgency(String name){
          return agencyDao.findAgency(name);
      }
     
       public void addGency(Agency a) {
           agencyDao.addGency(a); 
       }
}

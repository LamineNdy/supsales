/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supinfo.SupSalesAnalytics.dao.service;

import com.supinfo.SupSalesAnalytics.dao.SaleDao;
import com.supinfo.SupSalesAnalytics.entity.SaleEntity;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Lamine
 */

@Stateless
public class SaleService {
    @EJB
    private SaleDao saleDao;
    
    public void addSale(SaleEntity sale){
        saleDao.addSale(sale);
    }
    
    public List<SaleEntity> findSales(Integer saleId,Map<String,String> parameters){
        return saleDao.findSales(saleId,parameters);
    }
    
   public int getSoldQuantity(Map<String,String> parameters){
     return saleDao.getSoldQuantity(parameters);
    }
    
   public BigDecimal getSoldAmount(Map<String,String> parameters){
        return saleDao.getSoldAmount(parameters);
    }
    
    public Map<String, Integer> getChannelBreakDown(Map<String,String> parameters){
         return saleDao.getChannelBreakDown(parameters);
     }
    public Map<String, Integer> getAgencyBreakDown(Map<String,String> parameters) {
          return saleDao.getAgencyBreakDown(parameters);
      }
      
    public Map<String, Integer> getBreakDownByCustomerGender(Map<String,String> parameters) {
          return saleDao.getBreakDownByCustomerGender(parameters);
      }
      
    public Map<String, Integer> getBreakDownByCustomerMarital(Map<String,String> parameters){
           return saleDao.getBreakDownByCustomerMarital(parameters);
       }
       
     public Map<String, Integer> getBreakDownByCustomerIncome(Map<String,String> parameters) {
         return  saleDao.getBreakDownByCustomerIncome(parameters);
     }
     
     public Map<String, Integer> getCustomerLoyalty(Map<String,String> parameters) {
          return  saleDao.getCustomerLoyalty(parameters);
      }
     
      public Map<String,Integer> getTopProduct(Map<String,String> parameters) {
         return saleDao.getTopProducts(parameters);
     }
}

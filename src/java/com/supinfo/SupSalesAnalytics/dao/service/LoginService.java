/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supinfo.SupSalesAnalytics.dao.service;

import com.supinfo.SupSalesAnalytics.dao.BankAdvisorDao;
import com.supinfo.SupSalesAnalytics.entity.BankAdvisor;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Lamine
 */
@Stateless
public class LoginService {
    
    @EJB
    private BankAdvisorDao bankAdvisorDao;
    
     public void addBankAdvisor(BankAdvisor bankAdvisor) {
         bankAdvisorDao.addBankAdvisor(bankAdvisor);
    }
    
     public List<BankAdvisor> getAllBankAdvisor(){
         return bankAdvisorDao.getAllBankAdvisor();
     }
}

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.supinfo.SupSalesAnalytics.dao.service;

import com.supinfo.SupSalesAnalytics.dao.ProductDao;
import com.supinfo.SupSalesAnalytics.entity.ProductEntity;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author Lamine
 */
@Stateless
public class ProductService {
    
    @EJB
    ProductDao productDao;
    
    public void addProduct(ProductEntity pd){
        productDao.addProduct(pd);
    }
    
    public List<ProductEntity> findProduct(Integer prodId,Map<String,String> parameters){
     return productDao.findProduct(prodId,parameters);  
    }
    
    
}


package com.supinfo.SupSalesAnalytics.WSDL;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for channel complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="channel">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="channelClass" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="channelClassId" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="channelDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="channelId" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="channelTotal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="channelTotalId" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "channel", propOrder = {
    "channelClass",
    "channelClassId",
    "channelDesc",
    "channelId",
    "channelTotal",
    "channelTotalId"
})
public class Channel {

    protected String channelClass;
    protected Integer channelClassId;
    protected String channelDesc;
    protected Integer channelId;
    protected String channelTotal;
    protected Integer channelTotalId;

    /**
     * Gets the value of the channelClass property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChannelClass() {
        return channelClass;
    }

    /**
     * Sets the value of the channelClass property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChannelClass(String value) {
        this.channelClass = value;
    }

    /**
     * Gets the value of the channelClassId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getChannelClassId() {
        return channelClassId;
    }

    /**
     * Sets the value of the channelClassId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setChannelClassId(Integer value) {
        this.channelClassId = value;
    }

    /**
     * Gets the value of the channelDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChannelDesc() {
        return channelDesc;
    }

    /**
     * Sets the value of the channelDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChannelDesc(String value) {
        this.channelDesc = value;
    }

    /**
     * Gets the value of the channelId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getChannelId() {
        return channelId;
    }

    /**
     * Sets the value of the channelId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setChannelId(Integer value) {
        this.channelId = value;
    }

    /**
     * Gets the value of the channelTotal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChannelTotal() {
        return channelTotal;
    }

    /**
     * Sets the value of the channelTotal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChannelTotal(String value) {
        this.channelTotal = value;
    }

    /**
     * Gets the value of the channelTotalId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getChannelTotalId() {
        return channelTotalId;
    }

    /**
     * Sets the value of the channelTotalId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setChannelTotalId(Integer value) {
        this.channelTotalId = value;
    }

}

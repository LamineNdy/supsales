
package com.supinfo.SupSalesAnalytics.WSDL;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for product complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="product">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="prodCategory" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="prodCategoryDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="prodCategoryId" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="prodDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="prodEffFrom" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="prodEffTo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="prodId" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="prodListPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="prodMinPrice" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="prodName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="prodPackSize" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="prodSrcId" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="prodStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="prodSubcategory" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="prodSubcategoryDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="prodSubcategoryId" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="prodTotal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="prodTotalId" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="prodUnitOfMeasure" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="prodValid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="prodWeightClass" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="supplierId" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "product", propOrder = {
    "prodCategory",
    "prodCategoryDesc",
    "prodCategoryId",
    "prodDesc",
    "prodEffFrom",
    "prodEffTo",
    "prodId",
    "prodListPrice",
    "prodMinPrice",
    "prodName",
    "prodPackSize",
    "prodSrcId",
    "prodStatus",
    "prodSubcategory",
    "prodSubcategoryDesc",
    "prodSubcategoryId",
    "prodTotal",
    "prodTotalId",
    "prodUnitOfMeasure",
    "prodValid",
    "prodWeightClass",
    "supplierId"
})
public class Product {

    protected String prodCategory;
    protected String prodCategoryDesc;
    protected Integer prodCategoryId;
    protected String prodDesc;
    protected String prodEffFrom;
    protected String prodEffTo;
    protected Integer prodId;
    protected BigDecimal prodListPrice;
    protected BigDecimal prodMinPrice;
    protected String prodName;
    protected String prodPackSize;
    protected Integer prodSrcId;
    protected String prodStatus;
    protected String prodSubcategory;
    protected String prodSubcategoryDesc;
    protected Integer prodSubcategoryId;
    protected String prodTotal;
    protected Integer prodTotalId;
    protected String prodUnitOfMeasure;
    protected String prodValid;
    protected Integer prodWeightClass;
    protected Integer supplierId;

    /**
     * Gets the value of the prodCategory property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProdCategory() {
        return prodCategory;
    }

    /**
     * Sets the value of the prodCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProdCategory(String value) {
        this.prodCategory = value;
    }

    /**
     * Gets the value of the prodCategoryDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProdCategoryDesc() {
        return prodCategoryDesc;
    }

    /**
     * Sets the value of the prodCategoryDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProdCategoryDesc(String value) {
        this.prodCategoryDesc = value;
    }

    /**
     * Gets the value of the prodCategoryId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getProdCategoryId() {
        return prodCategoryId;
    }

    /**
     * Sets the value of the prodCategoryId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setProdCategoryId(Integer value) {
        this.prodCategoryId = value;
    }

    /**
     * Gets the value of the prodDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProdDesc() {
        return prodDesc;
    }

    /**
     * Sets the value of the prodDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProdDesc(String value) {
        this.prodDesc = value;
    }

    /**
     * Gets the value of the prodEffFrom property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProdEffFrom() {
        return prodEffFrom;
    }

    /**
     * Sets the value of the prodEffFrom property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProdEffFrom(String value) {
        this.prodEffFrom = value;
    }

    /**
     * Gets the value of the prodEffTo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProdEffTo() {
        return prodEffTo;
    }

    /**
     * Sets the value of the prodEffTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProdEffTo(String value) {
        this.prodEffTo = value;
    }

    /**
     * Gets the value of the prodId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getProdId() {
        return prodId;
    }

    /**
     * Sets the value of the prodId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setProdId(Integer value) {
        this.prodId = value;
    }

    /**
     * Gets the value of the prodListPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getProdListPrice() {
        return prodListPrice;
    }

    /**
     * Sets the value of the prodListPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setProdListPrice(BigDecimal value) {
        this.prodListPrice = value;
    }

    /**
     * Gets the value of the prodMinPrice property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getProdMinPrice() {
        return prodMinPrice;
    }

    /**
     * Sets the value of the prodMinPrice property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setProdMinPrice(BigDecimal value) {
        this.prodMinPrice = value;
    }

    /**
     * Gets the value of the prodName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProdName() {
        return prodName;
    }

    /**
     * Sets the value of the prodName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProdName(String value) {
        this.prodName = value;
    }

    /**
     * Gets the value of the prodPackSize property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProdPackSize() {
        return prodPackSize;
    }

    /**
     * Sets the value of the prodPackSize property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProdPackSize(String value) {
        this.prodPackSize = value;
    }

    /**
     * Gets the value of the prodSrcId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getProdSrcId() {
        return prodSrcId;
    }

    /**
     * Sets the value of the prodSrcId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setProdSrcId(Integer value) {
        this.prodSrcId = value;
    }

    /**
     * Gets the value of the prodStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProdStatus() {
        return prodStatus;
    }

    /**
     * Sets the value of the prodStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProdStatus(String value) {
        this.prodStatus = value;
    }

    /**
     * Gets the value of the prodSubcategory property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProdSubcategory() {
        return prodSubcategory;
    }

    /**
     * Sets the value of the prodSubcategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProdSubcategory(String value) {
        this.prodSubcategory = value;
    }

    /**
     * Gets the value of the prodSubcategoryDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProdSubcategoryDesc() {
        return prodSubcategoryDesc;
    }

    /**
     * Sets the value of the prodSubcategoryDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProdSubcategoryDesc(String value) {
        this.prodSubcategoryDesc = value;
    }

    /**
     * Gets the value of the prodSubcategoryId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getProdSubcategoryId() {
        return prodSubcategoryId;
    }

    /**
     * Sets the value of the prodSubcategoryId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setProdSubcategoryId(Integer value) {
        this.prodSubcategoryId = value;
    }

    /**
     * Gets the value of the prodTotal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProdTotal() {
        return prodTotal;
    }

    /**
     * Sets the value of the prodTotal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProdTotal(String value) {
        this.prodTotal = value;
    }

    /**
     * Gets the value of the prodTotalId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getProdTotalId() {
        return prodTotalId;
    }

    /**
     * Sets the value of the prodTotalId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setProdTotalId(Integer value) {
        this.prodTotalId = value;
    }

    /**
     * Gets the value of the prodUnitOfMeasure property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProdUnitOfMeasure() {
        return prodUnitOfMeasure;
    }

    /**
     * Sets the value of the prodUnitOfMeasure property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProdUnitOfMeasure(String value) {
        this.prodUnitOfMeasure = value;
    }

    /**
     * Gets the value of the prodValid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getProdValid() {
        return prodValid;
    }

    /**
     * Sets the value of the prodValid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setProdValid(String value) {
        this.prodValid = value;
    }

    /**
     * Gets the value of the prodWeightClass property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getProdWeightClass() {
        return prodWeightClass;
    }

    /**
     * Sets the value of the prodWeightClass property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setProdWeightClass(Integer value) {
        this.prodWeightClass = value;
    }

    /**
     * Gets the value of the supplierId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getSupplierId() {
        return supplierId;
    }

    /**
     * Sets the value of the supplierId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setSupplierId(Integer value) {
        this.supplierId = value;
    }

}

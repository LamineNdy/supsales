
package com.supinfo.SupSalesAnalytics.WSDL;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for promotion complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="promotion">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="promoBeginDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="promoCategory" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="promoCategoryId" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="promoCost" type="{http://www.w3.org/2001/XMLSchema}decimal" minOccurs="0"/>
 *         &lt;element name="promoEndDate" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="promoId" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="promoName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="promoSubcategory" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="promoSubcategoryId" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="promoTotal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="promoTotalId" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "promotion", propOrder = {
    "promoBeginDate",
    "promoCategory",
    "promoCategoryId",
    "promoCost",
    "promoEndDate",
    "promoId",
    "promoName",
    "promoSubcategory",
    "promoSubcategoryId",
    "promoTotal",
    "promoTotalId"
})
public class Promotion {

    protected String promoBeginDate;
    protected String promoCategory;
    protected Integer promoCategoryId;
    protected BigDecimal promoCost;
    protected String promoEndDate;
    protected Integer promoId;
    protected String promoName;
    protected String promoSubcategory;
    protected Integer promoSubcategoryId;
    protected String promoTotal;
    protected Integer promoTotalId;

    /**
     * Gets the value of the promoBeginDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPromoBeginDate() {
        return promoBeginDate;
    }

    /**
     * Sets the value of the promoBeginDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPromoBeginDate(String value) {
        this.promoBeginDate = value;
    }

    /**
     * Gets the value of the promoCategory property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPromoCategory() {
        return promoCategory;
    }

    /**
     * Sets the value of the promoCategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPromoCategory(String value) {
        this.promoCategory = value;
    }

    /**
     * Gets the value of the promoCategoryId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPromoCategoryId() {
        return promoCategoryId;
    }

    /**
     * Sets the value of the promoCategoryId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPromoCategoryId(Integer value) {
        this.promoCategoryId = value;
    }

    /**
     * Gets the value of the promoCost property.
     * 
     * @return
     *     possible object is
     *     {@link BigDecimal }
     *     
     */
    public BigDecimal getPromoCost() {
        return promoCost;
    }

    /**
     * Sets the value of the promoCost property.
     * 
     * @param value
     *     allowed object is
     *     {@link BigDecimal }
     *     
     */
    public void setPromoCost(BigDecimal value) {
        this.promoCost = value;
    }

    /**
     * Gets the value of the promoEndDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPromoEndDate() {
        return promoEndDate;
    }

    /**
     * Sets the value of the promoEndDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPromoEndDate(String value) {
        this.promoEndDate = value;
    }

    /**
     * Gets the value of the promoId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPromoId() {
        return promoId;
    }

    /**
     * Sets the value of the promoId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPromoId(Integer value) {
        this.promoId = value;
    }

    /**
     * Gets the value of the promoName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPromoName() {
        return promoName;
    }

    /**
     * Sets the value of the promoName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPromoName(String value) {
        this.promoName = value;
    }

    /**
     * Gets the value of the promoSubcategory property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPromoSubcategory() {
        return promoSubcategory;
    }

    /**
     * Sets the value of the promoSubcategory property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPromoSubcategory(String value) {
        this.promoSubcategory = value;
    }

    /**
     * Gets the value of the promoSubcategoryId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPromoSubcategoryId() {
        return promoSubcategoryId;
    }

    /**
     * Sets the value of the promoSubcategoryId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPromoSubcategoryId(Integer value) {
        this.promoSubcategoryId = value;
    }

    /**
     * Gets the value of the promoTotal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPromoTotal() {
        return promoTotal;
    }

    /**
     * Sets the value of the promoTotal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPromoTotal(String value) {
        this.promoTotal = value;
    }

    /**
     * Gets the value of the promoTotalId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getPromoTotalId() {
        return promoTotalId;
    }

    /**
     * Sets the value of the promoTotalId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setPromoTotalId(Integer value) {
        this.promoTotalId = value;
    }

}

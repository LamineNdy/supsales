
package com.supinfo.SupSalesAnalytics.WSDL;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for customer complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="customer">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="countryId" type="{http://www.w3.org/2001/XMLSchema}long" minOccurs="0"/>
 *         &lt;element name="custCity" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="custCityId" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="custCreditLimit" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="custEffFrom" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="custEffTo" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="custEmail" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="custFirstName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="custGender" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="custId" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="custIncomeLevel" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="custLastName" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="custMainPhoneNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="custMaritalStatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="custPostalCode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="custSrcId" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="custStateProvince" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="custStateProvinceId" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="custStreetAddress" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="custTotal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="custTotalId" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *         &lt;element name="custValid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="custYearOfBirth" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "customer", propOrder = {
    "countryId",
    "custCity",
    "custCityId",
    "custCreditLimit",
    "custEffFrom",
    "custEffTo",
    "custEmail",
    "custFirstName",
    "custGender",
    "custId",
    "custIncomeLevel",
    "custLastName",
    "custMainPhoneNumber",
    "custMaritalStatus",
    "custPostalCode",
    "custSrcId",
    "custStateProvince",
    "custStateProvinceId",
    "custStreetAddress",
    "custTotal",
    "custTotalId",
    "custValid",
    "custYearOfBirth"
})
public class Customer {

    protected Long countryId;
    protected String custCity;
    protected Integer custCityId;
    protected Integer custCreditLimit;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar custEffFrom;
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar custEffTo;
    protected String custEmail;
    protected String custFirstName;
    protected String custGender;
    protected Integer custId;
    protected String custIncomeLevel;
    protected String custLastName;
    protected String custMainPhoneNumber;
    protected String custMaritalStatus;
    protected String custPostalCode;
    protected Integer custSrcId;
    protected String custStateProvince;
    protected Integer custStateProvinceId;
    protected String custStreetAddress;
    protected String custTotal;
    protected Integer custTotalId;
    protected String custValid;
    protected Integer custYearOfBirth;

    /**
     * Gets the value of the countryId property.
     * 
     * @return
     *     possible object is
     *     {@link Long }
     *     
     */
    public Long getCountryId() {
        return countryId;
    }

    /**
     * Sets the value of the countryId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Long }
     *     
     */
    public void setCountryId(Long value) {
        this.countryId = value;
    }

    /**
     * Gets the value of the custCity property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustCity() {
        return custCity;
    }

    /**
     * Sets the value of the custCity property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustCity(String value) {
        this.custCity = value;
    }

    /**
     * Gets the value of the custCityId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCustCityId() {
        return custCityId;
    }

    /**
     * Sets the value of the custCityId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCustCityId(Integer value) {
        this.custCityId = value;
    }

    /**
     * Gets the value of the custCreditLimit property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCustCreditLimit() {
        return custCreditLimit;
    }

    /**
     * Sets the value of the custCreditLimit property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCustCreditLimit(Integer value) {
        this.custCreditLimit = value;
    }

    /**
     * Gets the value of the custEffFrom property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCustEffFrom() {
        return custEffFrom;
    }

    /**
     * Sets the value of the custEffFrom property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCustEffFrom(XMLGregorianCalendar value) {
        this.custEffFrom = value;
    }

    /**
     * Gets the value of the custEffTo property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getCustEffTo() {
        return custEffTo;
    }

    /**
     * Sets the value of the custEffTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setCustEffTo(XMLGregorianCalendar value) {
        this.custEffTo = value;
    }

    /**
     * Gets the value of the custEmail property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustEmail() {
        return custEmail;
    }

    /**
     * Sets the value of the custEmail property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustEmail(String value) {
        this.custEmail = value;
    }

    /**
     * Gets the value of the custFirstName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustFirstName() {
        return custFirstName;
    }

    /**
     * Sets the value of the custFirstName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustFirstName(String value) {
        this.custFirstName = value;
    }

    /**
     * Gets the value of the custGender property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustGender() {
        return custGender;
    }

    /**
     * Sets the value of the custGender property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustGender(String value) {
        this.custGender = value;
    }

    /**
     * Gets the value of the custId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCustId() {
        return custId;
    }

    /**
     * Sets the value of the custId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCustId(Integer value) {
        this.custId = value;
    }

    /**
     * Gets the value of the custIncomeLevel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustIncomeLevel() {
        return custIncomeLevel;
    }

    /**
     * Sets the value of the custIncomeLevel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustIncomeLevel(String value) {
        this.custIncomeLevel = value;
    }

    /**
     * Gets the value of the custLastName property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustLastName() {
        return custLastName;
    }

    /**
     * Sets the value of the custLastName property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustLastName(String value) {
        this.custLastName = value;
    }

    /**
     * Gets the value of the custMainPhoneNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustMainPhoneNumber() {
        return custMainPhoneNumber;
    }

    /**
     * Sets the value of the custMainPhoneNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustMainPhoneNumber(String value) {
        this.custMainPhoneNumber = value;
    }

    /**
     * Gets the value of the custMaritalStatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustMaritalStatus() {
        return custMaritalStatus;
    }

    /**
     * Sets the value of the custMaritalStatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustMaritalStatus(String value) {
        this.custMaritalStatus = value;
    }

    /**
     * Gets the value of the custPostalCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustPostalCode() {
        return custPostalCode;
    }

    /**
     * Sets the value of the custPostalCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustPostalCode(String value) {
        this.custPostalCode = value;
    }

    /**
     * Gets the value of the custSrcId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCustSrcId() {
        return custSrcId;
    }

    /**
     * Sets the value of the custSrcId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCustSrcId(Integer value) {
        this.custSrcId = value;
    }

    /**
     * Gets the value of the custStateProvince property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustStateProvince() {
        return custStateProvince;
    }

    /**
     * Sets the value of the custStateProvince property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustStateProvince(String value) {
        this.custStateProvince = value;
    }

    /**
     * Gets the value of the custStateProvinceId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCustStateProvinceId() {
        return custStateProvinceId;
    }

    /**
     * Sets the value of the custStateProvinceId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCustStateProvinceId(Integer value) {
        this.custStateProvinceId = value;
    }

    /**
     * Gets the value of the custStreetAddress property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustStreetAddress() {
        return custStreetAddress;
    }

    /**
     * Sets the value of the custStreetAddress property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustStreetAddress(String value) {
        this.custStreetAddress = value;
    }

    /**
     * Gets the value of the custTotal property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustTotal() {
        return custTotal;
    }

    /**
     * Sets the value of the custTotal property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustTotal(String value) {
        this.custTotal = value;
    }

    /**
     * Gets the value of the custTotalId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCustTotalId() {
        return custTotalId;
    }

    /**
     * Sets the value of the custTotalId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCustTotalId(Integer value) {
        this.custTotalId = value;
    }

    /**
     * Gets the value of the custValid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustValid() {
        return custValid;
    }

    /**
     * Sets the value of the custValid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustValid(String value) {
        this.custValid = value;
    }

    /**
     * Gets the value of the custYearOfBirth property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getCustYearOfBirth() {
        return custYearOfBirth;
    }

    /**
     * Sets the value of the custYearOfBirth property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setCustYearOfBirth(Integer value) {
        this.custYearOfBirth = value;
    }

}

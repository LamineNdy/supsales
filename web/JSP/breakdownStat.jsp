<%-- 
    Document   : breakdownStat
    Created on : Feb 24, 2013, 7:08:34 PM
    Author     : Lamine
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <title>Dashboard</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href="../css/bootstrap.css" rel="stylesheet">
    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
      .sidebar-nav {
        padding: 9px 0;
      }

      @media (max-width: 980px) {
        /* Enable use of floated navbar text */
        .navbar-text.pull-right {
          float: none;
          padding-left: 5px;
          padding-right: 5px;
        }
      }
    </style>
    <link href="../css/bootstrap-responsive.css" rel="stylesheet">
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript">
      google.load("visualization", "1", {packages:["corechart"]});
     
        
            function dataLoad(temp) {
                var data = new google.visualization.DataTable();
                data.addColumn('string', 'Channels');
                data.addColumn('number', 'Breakdown');  
                var json=JSON.parse(temp);
                for (var i = 0; i < json.length; i++) {
                    var obj = json[i];
                    for (var key in obj) {
                        data.addRows([
                        [key, Number(obj[key])]
                        ]);                      
                    }
                }
                return data;
            }
            
      function drawChart(temp,element,title) { 
          
          var data=dataLoad(temp);         
       var options = {
          title: 'Breakdowns by '+ title,
          is3D: true          
        };
  
        var chart = new google.visualization.PieChart(document.getElementById(element));
        chart.draw(data, options);       
      }
    </script>
  </head>

 <body onload="drawChart('<c:out value="${Channelsbreakdowns}"/>','chart_div','channel');drawChart('<c:out value="${Agenciesbreakdowns}"/>','chart_div2','agency');">

    <div class="navbar navbar-inverse navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container-fluid">         
         <a class="brand" href="${dashboardUrl}">SupSale Analytics</a>
          <div class="nav-collapse collapse">             
             <p class="navbar-text pull-right">
              <c:url value="/Auth/Logout" var="Logout" />                  
              <a href="${Logout}" class="navbar-link">Log Out</a>
            </p>   
          </div><!--/.nav-collapse -->
        </div>
      </div>
    </div>

    <div class="container-fluid">
      <div class="row-fluid">
        <div class="span3">
          <div class="well sidebar-nav">
            <ul class="nav nav-list">             
              <li class="nav-header">Dashboard Stats</li>
              <li>
                  <c:url value="/Auth/Dashboard" var="dashboardUrl" />
                  <a href="${dashboardUrl}">Sales</a>
              </li>
              <li>
                <c:url value="/Auth/ProductStat" var="pstatUrl" />
                <a href="${pstatUrl}">Products</a>                  
              </li>
              <li>
                  <c:url value="/Auth/BreakdownStat" var="breakUrl" />
                  <a href="${breakUrl}">Breakdowns</a>
              </li>
              <li>
                  <c:url value="/Auth/CustomersStat" var="customerUrl" />
                  <a href="${customerUrl}">Customers</a>
              </li>                        
            </ul>
          </div><!--/.well -->
           
            <div class="well sidebar-nav">
               <ul class="nav nav-list">             
                <li class="nav-header">Filters (the filter affect the whole dashboard)</li>
                <li>
                   <select name="agency" id="agency" onchange="MM_jumpMenu('parent',this,0)" >
                        <option selected >--Agency--</option> 
                    <c:forEach items="${agencies}" var="agency">
                         <option value="${breakUrl}?agency=${agency.name}"> <c:out value="${agency.name}" /></option> 
                    </c:forEach>                                     
                  </select>
                </li>
                <li>
                   <select name="gender" id="gender" onchange="MM_jumpMenu('parent',this,0)" >
                       <option selected >--Gender--</option> 
                      <option value="${breakUrl}?gender=M"> <c:out value="Male" /></option>
                      <option value="${breakUrl}?gender=F"> <c:out value="Female" /></option>                                                         
                  </select>
                </li>
                 <li>
                   <select name="marital" id="marital" onchange="MM_jumpMenu('parent',this,0)" >
                       <option selected >--Marital Status--</option> 
                      <option value="${breakUrl}?marital=single"> <c:out value="single" /></option>
                      <option value="${breakUrl}?marital=married"> <c:out value="married" /></option> 
                      <option value="${breakUrl}?marital=divorced"> <c:out value="divorced" /></option> 
                  </select>
                </li>
                
                  <li>
                   <select name="income" id="income" onchange="MM_jumpMenu('parent',this,0)" >
                        <option selected >--Income level--</option> 
                    <c:forEach items="${incomeLevels}" var="income">
                         <option value="${breakUrl}?income=${income}"> <c:out value="${income}" /></option> 
                    </c:forEach>                                       
                  </select>
                </li>
                 <li>   <a href="Clear?page=BreakdownStat" class="btn btn-primary btn-large">Clear filter &raquo;</a></p></li>
               </ul>
           </div>
        </div><!--/span-->
        <div class="span9">
          <div id="chart_div" style="height: 500px;">                   
          </div>        
          </div><!--/row-->
           <div class="span9">
          <div id="chart_div2" style="height: 500px;">                   
          </div>        
          </div><!--/row-->
        </div><!--/span-->
      </div><!--/row-->

      <hr>

      <footer>
        <p>&copy; Company 2013</p>
      </footer>

    </div><!--/.fluid-container-->
      <script type="text/javascript">
function MM_jumpMenu(targ,selObj,restore){ //v3.0
  eval(targ+".location='"+selObj.options[selObj.selectedIndex].value+"'");
  if (restore) selObj.selectedIndex=0;
}
</script>
  </body>
</html>

